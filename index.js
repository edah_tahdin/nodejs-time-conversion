const chalk = require("chalk");
const figlet = require("figlet");
const prompt = require("prompt-sync")({
  sigint: true
});

const init = () => {
  console.log(chalk.cyan(figlet.textSync("Time")));
  console.log(chalk.cyan(figlet.textSync("Conversion")));
  console.log(chalk.cyan("by AD Syerit Zulfinda Tahdin"));
}

const inputTime = () => {
  let s = prompt("Masukkan waktu yang akan dikonversi (Format. hh:mm:ssAM or hh:mm:ssPM): ");
  if (s === "") inputTime();

  let isValid = /((1[0-2]|0[1-9]):([0-5][0-9]):([0-5][0-9])?([AP][M]))/.test(s);
  if (isValid === false) {
    console.log(chalk.black.bgRedBright("Format inputan salah"));
    inputTime();
  }

  let conversion = timeConversion(s);
  console.log(chalk.black.bgCyanBright("Hasil konversi: " + conversion));
  stopApp();
}

const timeConversion = (s) => {
  let hour = s.substring(0, 2) * 1;
  let timeFormat = s.substring(2, 8);

  if (hour === 12 && s.indexOf("AM") !== -1) return ("00" + timeFormat);

  if (hour === 12 && s.indexOf("PM") !== -1) return ("12" + timeFormat);

  if (hour < 12 && s.indexOf("PM") !== -1) {
    return (12 + hour + timeFormat);
  } else {
    if (hour < 10) return ("0" + hour + timeFormat);
    else return (hour + timeFormat);
  }
}

const stopApp = () => {
  let answer = prompt("Konversi lagi? (Y/N): ");
  if (answer === "Y") inputTime();
  else if (answer === "N") console.log(chalk.black.bgCyanBright("Terima kasih"));
  else stopApp();
}

const run = async () => {
  init();
  inputTime();
}

run();